#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double norma2(int m, double x[])
{
	double res=0;
	for(int i = 0; i < m; i++)
	{
		res = res + x[i]*x[i];
		//printf("res = %f \n", res);
	}
	res = sqrt(res);
	return res;
}

int main(int argc, char** argv)
{
	int m = 0, n = 0, i = 0, j = 0;
	
	printf("Enter number of strings: m = ");
	scanf("%d", &m);
	printf("Enter number of strings: n = ");
	scanf("%d", &n);
	
	double **A = (double**)malloc(n*sizeof(double*));
	for(i = 0; i < n; i++)
		A[i] = (double*)malloc(m*sizeof(double));
	
	for(i = 0; i < n; i++)
		for(j = 0; j < m; j++)
			A[i][j] = rand();
	
	double u[m], v[n], x[m], y[n];
	double dopvector1[n], dopvector2[m];
	double **dopmatrix1 = (double**)malloc(n*sizeof(double*));
	for(i = 0; i < n; i++)
		dopmatrix1[i] = (double*)malloc(m*sizeof(double));

	int k;
	int eu1[m], ev1[n-1];

	for(i = 0; i < m; i++)
		dopvector1[i] = eu1[i] = dopvector1[i] = 0;
	eu1[0] = 1;
	
	for(i = 0; i < n-1; i++)
		dopvector2[i] = ev1[i] = dopvector2[i] = 0;
	ev1[0] = 1;

	for(k = 0; k < n; k++)
	{
		for(i = k; i < m; i++)
			x[i] = A[i][k];
					
		for(i = k; i < m; i++)
			u[i] = x[i] - norma2(m - k, x + k) * eu1[i - k];
		
		double norm2u = norma2(m - k, u + k);
		
		if(norm2u != 0)
			for(i = k; i < m; i++)
				u[i] = u[i]/norm2u;
	
		for(i = k; i < n; i++)
		{
			dopvector1[i] = 0;
			for(j = k; j < m; j++)
				dopvector1[i] = dopvector1[i] + u[j]*A[j][i];
		}
		
		for(i = k; i < m; i++)
			for(j = k; j < n; j++)
				dopmatrix1[i][j] = u[i]*dopvector1[j];
			
		for(i = k; i < m; i++)
			for(j = k; j < n; j++)
				A[i][j] = A[i][j] - 2*dopmatrix1[i][j];
			
		
		if(k < n-2)
		{
			for(j = k+1; j < n; j++)
				y[j] = A[k][j];
		
			double norm2y = norma2(n - k - 1 , y + k + 1);
			
			for(i = k + 1; i < n; i++)
				v[i] = y[i] - norm2y*ev1[i-k-1];
		
			double norm2v = norma2(n - k - 1, v + k + 1);
			
			for(i = k + 1; i < n; i++)
				v[i] = v[i]/norm2v;
			
			for(i = k; i < m; i++)
			{
				dopvector2[i] = 0;
				for(j = k + 1; j < n; j++)
					dopvector2[i] = dopvector2[i] + A[i][j]*v[j];
			}
			
			for(i = k; i < m; i++)
				for(j = k + 1; j < n; j++)
					dopmatrix1[i][j] = dopvector2[i]*v[j];
			
			for(i = k; i < m; i++)
				for(j = k + 1; j < n; j++)
					A[i][j] = A[i][j] - 2*dopmatrix1[i][j];			
		}
	}	
	
	FILE *res = fopen("result.txt", "w");
	
	for(i = 0; i < m; i++)
	{
		for(j = 0; j < n; j++)
		{
			fprintf(res, "%6.4lf ", A[i][j]); 
		}
		fprintf(res, "\n"); 
	}
		
	return 0;
}
