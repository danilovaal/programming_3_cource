program bid
	implicit none
	
	integer m, n, k, i, j
	parameter (m = 1024, n = 1024)
	real*8 :: A(m, n), u(m), v(n), vect1(n), vect2(m)
	real*8 norm
	double precision start_time, end_time
	
	call random_number(A) 
	
	Call cpu_time(start_time)
	do k = 1, n
		
		u(k:m) = A(k:m,k)
		norm = norm2(u(k:m))
		u(k) = u(k) - norm
		norm = norm2(u(k:m))
		
		if (norm > 0) then
			u(k:m) = u(k:m) / norm
		endif
		
		
		do i = k, n
			vect1(i) = 0
			do j = k, m
				vect1(i) = vect1(i) + u(j)*A(j, i)
			enddo
		enddo
		
		do j = k, n
			do i = k, m
				A(i, j) = A(i, j) - 2 * u(i) * vect1(j)
			enddo
		enddo
		
		if(k <= n-2) then
			v(k + 1:n) = A(k, k + 1:n)
			norm = norm2(v(k + 1:n))
			v(k + 1) = v(k + 1) - norm
			norm = norm2(v(k + 1:n))
			
			v(k + 1:n) = v(k + 1:n) / norm
			
			do i = k, m
				vect2(i) = 0
				do j = k + 1, n
					vect2(i) = vect2(i) + A(i, j)*v(j)
				enddo
			enddo
			
			do j = k + 1, m
				do i = k, n
					A(i, j) = A(i, j) - 2 * vect2(i) * v(j) 
				enddo
			enddo
			
		endif
	enddo
	Call cpu_time(end_time)
	
	
	print '(f10.4)', end_time - start_time
		
end program bid
