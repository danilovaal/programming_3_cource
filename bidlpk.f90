program bid
	implicit none
	
	integer m, n, k, info, lwork
	parameter (m = 2048, n = 2048)
	real*8 :: A(m, n), D(n), E(n-1), TQ(n), TP(n)
	real*8 :: w
	real*8, allocatable :: work(:)
	double precision start_time, end_time
	
	call random_number(A) 
	
	lwork = -1
	call dgebrd(m, n, A, m, D, E, TQ, TP, w, lwork, info)
	lwork = w
	allocate(work(lwork))
	
	Call cpu_time(start_time)
	
	call dgebrd(m, n, A, m, D, E, TQ, TP, work, lwork, info)
	
	Call cpu_time(end_time)
	
	print '(f10.4)', end_time - start_time
		
end program bid
